import { Form } from "../../js/form/form"

export class AddProductComponent 
{
    /**
     * Initializes a new instance of the constructor.
     *
     * @param {type} paramName - description of parameter
     * @return {type} description of return value
     */
    constructor()
    {
        this.#FormConstructor()
    }

    /**
    * Constructs a form and appends it to the document body.
    */
    #FormConstructor() 
    {
        const form = new Form()
        const formElement = form.generateForm()
        document.body.appendChild(formElement)

        const inputs = formElement.querySelectorAll('input')
        const submitButton = formElement.querySelector('#submit_btn')
        
        inputs.forEach(input => {
            input.addEventListener('input', () => this.handleState(inputs, submitButton))
        })
    }

    /**
     * Handles the state of the inputs and the submit button.
     *
     * @param {Array} inputs - An array of input elements.
     * @param {Element} submitButton - The submit button element.
     */
    handleState(inputs, submitButton) 
    {
        const [input1, input2] = inputs

        if (!input1.value || !input2.value) 
        {
            submitButton.disabled = true
        } else {
            submitButton.disabled = false
        }
    }
}