import { ToasterBox } from "./toasterBox"

export class ToasterInfo extends ToasterBox {
    #document = document.querySelector('body')
    showInfo(message) {
        this.buildToast("Info")

        const content = this.#document.querySelector('.toaster p')
        content.innerHTML = message
        
        this.dismiss(3)
    }
}