import './toaster.css'

export class ToasterBox
{
    #document = document.querySelector('body')
  
    constructor() 
    {
        this.#buildLayout()
    }

    #buildStyle()
    {
        const styleSheet = document.createElement('link')
        styleSheet.setAttribute('href', '/src/js/toaster/toaster.css')
        styleSheet.setAttribute('rel', 'stylesheet')
        document.querySelector('head').appendChild(styleSheet)
    }

    #buildToast()
    {
        const toast = document.createElement('div')
        const content = document.createElement('p')

        content.innerHTML = "Initial Toast"

        toast.classList.add('toaster')
        
        this.#document.appendChild(toast)
        toast.appendChild(content)
    }

    dismiss(duration = 0)
    {
        setTimeout(
            () => document.querySelector('.toaster').remove(),
            duration * 1000
        )
    }

    #buildLayout()
    {
        this.#buildStyle()
        this.#buildToast()
    }
}