export class PictureLoader
{
    #document = document.querySelector('body')

    constructor()
    {
        this.#buildLayout()
    }

    dismiss(duration = 0)
    {
        setTimeout(
            () => document.querySelector('.outer-box').remove(),
            duration * 1000
        )
    }

    #buildLayout()
    {
        const styleSheet = document.createElement('link')
        styleSheet.setAttribute('href', '/src/js/loader/loader.css')
        styleSheet.setAttribute('rel', 'stylesheet')

        document.querySelector('head').appendChild(styleSheet)

        const outerBox = document.createElement('div')
        outerBox.classList.add('outer-box')

        const innerBox = document.createElement('div')
        innerBox.classList.add('inner-box')

        const picture = document.createElement('img')
        picture.src = '/assets/images/loader/loader.webp'
        innerBox.appendChild(picture)
        
        outerBox.appendChild(innerBox)
        this.#document.appendChild(outerBox)
    }
}