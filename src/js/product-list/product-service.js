import { Config } from "../config/config.js"
import { HttpClient } from "../http/http-client.js"
import { ProductDeserializer } from "./product-deserializer.js"

export class ProductService {
    /**
     * Name of the localStorage key
     * @var string
     */
    #productKey = 'product'
    
    #httpClient = null

    constructor() {
        this.#httpClient = new HttpClient()
    }
    /**
     * Returns all products for the defined key
     * @returns array
     */
    async findAll() {
        const payload = await this.#httpClient.get(
            Config.API_ROUTES.get('all_product')
        )
        
        return ProductDeserializer.deserializeArray(payload)
    }

    /**
     * Find one product by his id
     * @param {string} id 
     */
    async findOne(id) {}

    async add(item) {}

    async remove(id) {
        const allProducts = await this.findAll()
        const filteredProducts = allProducts.filter(product => product.id !== id)
        localStorage.setItem(this.#productKey, JSON.stringify(filteredProducts))
    }
}