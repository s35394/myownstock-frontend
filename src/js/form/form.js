// import './form.css'

export class Form {
    constructor() {
      this.form = this.generateForm()
    }
  
    /**
     * Generates a form element with input fields for ID, label, stock, and a submit button.
     *
     * @return {HTMLFormElement} The generated form element.
     */
    generateForm() 
    {
      const form = document.createElement("form")
      form.classList.add("form")
  
      const IDinput = this.createInput("id", "text", "ID", true)
      const labelInput = this.createInput("label", "text", "Label/Name", true)
      const stockInput = this.createInput("stock", "number", "Stock", true)
      stockInput.addEventListener("input", () =>
        this.handleStockInput(stockInput)
      )
  
      const submitButton = this.createButton(
        "submit_btn",
        "submit",
        "Submit",
        true
      )
  
      form.append(IDinput, labelInput, stockInput, submitButton)
  
      return form
    }
  
    /**
     * Creates an input element with the specified attributes.
     *
     * @param {string} id - The ID of the input element.
     * @param {string} type - The type of the input element.
     * @param {string} placeholder - The placeholder text of the input element.
     * @param {boolean} required - Specifies if the input element is required.
     * @return {HTMLElement} - The created input element.
     */
    createInput(id, type, placeholder, required) 
    {
      const input = document.createElement("input")
      input.id = id
      input.type = type
      input.placeholder = placeholder
      input.required = required
      return input
    }
  
    /**
     * Creates a button element with the specified id, type, text content, and disabled state.
     *
     * @param {string} id - The id of the button.
     * @param {string} type - The type of the button.
     * @param {string} textContent - The text content of the button.
     * @param {boolean} disabled - The disabled state of the button.
     * @return {HTMLButtonElement} - The created button element.
     */
    createButton(id, type, textContent, disabled) 
    {
      const button = document.createElement("button")
      button.id = id
      button.type = type
      button.textContent = textContent
      button.disabled = disabled
      return button
    }
  
    handleStockInput(stockInput) 
    {
      if (stockInput.value < "0") 
      {
        stockInput.value = "0"
      }
    }
  
    getForm() 
    {
      return this.form
    }
  }