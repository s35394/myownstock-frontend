import { Form } from "../src/js/form/form"
import { AddProductComponent } from "../src/components/add/add.component"

describe('code snippet', () => {

    const code_under_test = new AddProductComponent()

    it('should generate and append form to the document body', () => {
      const formConstructorSpy = jest.spyOn(code_under_test, '#FormConstructor')
      const formGenerateFormSpy = jest.spyOn(code_under_test.Form.prototype, 'generateForm')
      const appendChildSpy = jest.spyOn(document.body, 'appendChild')

      code_under_test.FormConstructor()

      expect(formConstructorSpy).toHaveBeenCalled()
      expect(formGenerateFormSpy).toHaveBeenCalled()
      expect(appendChildSpy).toHaveBeenCalled()
    })

    // All input fields are generated with correct attributes
    it('should generate input fields with correct attributes', () => {
      const formConstructorSpy = jest.spyOn(code_under_test, '#FormConstructor')
      const formGenerateFormSpy = jest.spyOn(code_under_test.Form.prototype, 'generateForm')

      code_under_test.FormConstructor()

      expect(formConstructorSpy).toHaveBeenCalled()
      expect(formGenerateFormSpy).toHaveBeenCalled()

      const formElement = document.querySelector('form')
      const idInput = formElement.querySelector('#id')
      const labelInput = formElement.querySelector('#label')
      const stockInput = formElement.querySelector('#stock')

      expect(idInput).toBeTruthy()
      expect(idInput.type).toBe('text')
      expect(idInput.placeholder).toBe('ID')
      expect(idInput.required).toBe(true)

      expect(labelInput).toBeTruthy()
      expect(labelInput.type).toBe('text')
      expect(labelInput.placeholder).toBe('Label/Name')
      expect(labelInput.required).toBe(true)

      expect(stockInput).toBeTruthy()
      expect(stockInput.type).toBe('number')
      expect(stockInput.placeholder).toBe('Stock')
      expect(stockInput.required).toBe(true)
    })

    // Submit button is generated with correct attributes
    it('should generate submit button with correct attributes', () => {
      const formConstructorSpy = jest.spyOn(code_under_test, '#FormConstructor')
      const formGenerateFormSpy = jest.spyOn(code_under_test.Form.prototype, 'generateForm')

      code_under_test.FormConstructor()

      expect(formConstructorSpy).toHaveBeenCalled()
      expect(formGenerateFormSpy).toHaveBeenCalled()

      const formElement = document.querySelector('form')
      const submitButton = formElement.querySelector('#submit_btn')

      expect(submitButton).toBeTruthy()
      expect(submitButton.type).toBe('submit')
      expect(submitButton.textContent).toBe('Submit')
    })

    // Form is not generated if Form constructor is not called
    it('should not generate form if Form constructor is not called', () => {
      const formGenerateFormSpy = jest.spyOn(code_under_test.Form.prototype, 'generateForm')

      expect(formGenerateFormSpy).not.toHaveBeenCalled()

      const formElement = document.querySelector('form')
      expect(formElement).toBeFalsy()
    })

    // Form is not appended to the document body if Form constructor is not called
    it('should not append form to the document body if Form constructor is not called', () => {
      const appendChildSpy = jest.spyOn(document.body, 'appendChild')

      expect(appendChildSpy).not.toHaveBeenCalled()
    })

    // Input event listeners are not added if Form constructor is not called
    it('should not add input event listeners if Form constructor is not called', () => {
      const formConstructorSpy = jest.spyOn(code_under_test, '#FormConstructor')
      const inputs = document.querySelectorAll('input')

      expect(formConstructorSpy).not.toHaveBeenCalled()

      inputs.forEach(input => {
        expect(input.oninput).toBeFalsy()
      })
    })
})
