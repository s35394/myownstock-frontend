describe('AddProductComponent', () => 
{

    beforeEach(() => 
    {
        cy.visit('localhost:8081')

    })

    it('should enable submit button when inputs have values', () => 
    {
        cy.get('#id').type('123')
        cy.get('#label').type('Product Name')
        cy.get('#stock').type('10')

        cy.get('#submit_btn').should('not.be.disabled')
    })

    it('should disable submit button when inputs are empty', () => 
    {
        cy.get('#submit_btn').should('be.disabled')
    })

    it('should return to 0 if the stock input is negative', () => 
    {
        cy.get('#stock').type('-5')
        cy.get('#stock').should('have.value', '0')
    })
})